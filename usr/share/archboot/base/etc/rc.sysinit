#! /bin/bash
. /etc/rc.conf
. /etc/rc.d/functions

# needed in rc.sysinit, no fstab used!
/bin/mkdir -p /run/lock /dev/{pts,shm}
/bin/chmod 1777 /run/lock
/bin/mountpoint -q /dev/pts || /bin/mount /dev/pts &> /dev/null \
	|| /bin/mount -n -t devpts devpts /dev/pts -o mode=620,gid=5,nosuid,noexec
/bin/mountpoint -q /dev/shm || /bin/mount /dev/shm &> /dev/null \
	|| /bin/mount -n -t tmpfs shm /dev/shm -o mode=1777,nosuid,nodev

echo " "
printhl "Arch Linux\n"
printhl "${C_H2}http://www.archlinux.org"
printsep

run_hook sysinit_start

# bring up the loopback interface
[[ -d /sys/class/net/lo ]] && \
    status "Bringing up loopback interface" /sbin/ifconfig lo 127.0.0.1 up

if [[ $HOSTNAME ]]; then
	stat_busy "Setting Hostname: $HOSTNAME"
		echo $HOSTNAME > /proc/sys/kernel/hostname
	stat_done
fi

if [[ -e /proc/sys/kernel/dmesg_restrict && $(< /proc/sys/kernel/dmesg_restrict) -eq 1 ]]; then
    : >| /var/log/dmesg.log
    chmod 600 /var/log/dmesg.log
else
    : >| /var/log/dmesg.log
    chmod 644 /var/log/dmesg.log
fi
dmesg >| /var/log/dmesg.log

# Load sysctl variables if sysctl.conf is present
[[ -r /etc/sysctl.conf ]] && /sbin/sysctl -q -p &>/dev/null

# Start daemons
for daemon in "${DAEMONS[@]}"; do
    case ${daemon:0:1} in
        '!') continue;;     # Skip this daemon.
        '@') start_daemon_bkgd "${daemon#@}";;
        *)   start_daemon "$daemon";;
    esac
done

if [[ -x /etc/rc.local ]]; then
  /etc/rc.local
fi

run_hook sysinit_end
